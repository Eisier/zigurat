$(document).ready(function(){

    $('#logIn').click(function(e){
        e.preventDefault();

        var name = $("#name").val();
        var password = $("#password").val();
        
        // validaciones
        if(!(name.includes('@'))){
            $('#resultado').addClass('error').html('Un email tiene que contener un @');
            $('.container').effect('shake');
            return false;
        }
        
        if(password.length < 4 ){
            $('#resultado').addClass('error').html('La contraseña tiene que tener más de 4 caracteres');
            $('.container').effect('shake');
            return false;
        }
        //...

        $.ajax({
            url: "action.php",
            type: "POST",
            data: {
                name : name,
                password : password,
            },
            success: function(data){
                $('#resultado').removeClass('error').html(data);
            }
        })
        
    });

});

