<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Usuario;
use App\Models\Source;


class Usuarios extends Controller{

    public function index(){

        $usuario = new Usuario();
        
        $admin = $usuario->where([
            'email' => $this->request->getVar('email'),
            'password' => $this->request->getVar('password'),
            'rol' => 'admin',
        ])->first();

        if($admin){
            $datos['usuarios'] = $usuario->orderBy('id', 'ASC')->findAll();

            $datos['header'] = view('templates/header');
            $datos['footer'] = view('templates/footer');
            
            return view('usuarios/listado', $datos);
        }else{
           
            return view('usuarios/index');
        }

    }

    public function listado(){
        
        $usuario = new Usuario();
        $datos['usuarios'] = $usuario->orderBy('id', 'ASC')->findAll();

        $datos['header'] = view('templates/header');
        $datos['footer'] = view('templates/footer');
        
        return view('usuarios/listado', $datos);
    }

    public function crear(){

        return view('usuarios/crear');
    }

    public function guardar(){
        
        $validacion = $this->validate([
            'nombre' => 'required|min_length[3]',
            'email' => 'required|valid_email'
        ]);

        if($this->request && $validacion){
            
            $usuario = new Usuario();

            $source = "";
            foreach($this->request->getVar('source') as $item) $source .= $item . ", ";
            
            $datos = [
                'nombre' => $this->request->getVar('nombre'),
                'email' => $this->request->getVar('email'),
                'telefono' => $this->request->getVar('telefono'),
                'pais' => $this->request->getVar('pais'),
                'campaign' => $this->request->getVar('campaign'),
                'source' => substr($source, 0, -2),
                'medium' => $this->request->getVar('medium')
            ];
            if($usuario->insert($datos)){
                echo "Guardado con éxito";
                return $this->response->redirect('listado');
            }else{
                session()->setFlashdata('mensaje', 'Información incorrecta');
                return redirect()->back()->withInput();
            }
        }else{

            session()->setFlashdata('mensaje', 'Información incorrecta');
            return redirect()->back()->withInput();
        }
        
    }

    public function borrar($id){

        $usuario = new Usuario();
        $query = $usuario->where('id', $id); 
        
        if($query->delete($id)){
            return $this->response->redirect('../listado');
        }
    }

    public function editar($id){

        $usuario = new Usuario();
        $source = new Source();

        $query['usuario'] = $usuario->where('id', $id)->first();
        //$query['source'] = $source->all();

        return view('usuarios/editar', $query);
    }

    public function actualizar(){
        
        $validacion = $this->validate([
            'nombre' => 'required|min_length[3]',
            'email' => 'required|valid_email'
        ]);

        if($this->request && $validacion){
            $usuario = new Usuario();
            $source = "";

            if($this->request->getVar('source')){
                foreach($this->request->getVar('source') as $item){
                    $source .= $item . ", ";
                }
            }

            $datos = [
                'nombre' => $this->request->getVar('nombre'),
                'email' => $this->request->getVar('email'),
                'telefono' => $this->request->getVar('telefono'),
                'pais' => $this->request->getVar('pais'),
                'campaign' => $this->request->getVar('campaign'),
                'source' => substr($source, 0, -2),
                'medium' => $this->request->getVar('medium')
            ];

            if($usuario->update($this->request->getVar('id'), $datos)){
                echo 'Actualizado con éxito';
                return $this->response->redirect('listado');
            }else{
                session()->setFlashdata('mensaje', 'Información incorrecta');
                return redirect()->back()->withInput();
            }
        }else{
            session()->setFlashdata('mensaje', 'Información incorrecta');
            return redirect()->back()->withInput();
        }
    }
}