<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <script src="../js/jquery-3.6.0.min.js"></script>
    <script src="../js/jquery-ui.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/styles.css" >
    <title>Document Title</title>
</head>
    <body>
        <div class="div-center">
            <div class="container">
                <form action = 'index' id = 'form' method = 'post' enctype="multipart/form-data">
                    <h1 class="display-4">LOG IN</h1>
                    <div class="form-group row">
                        <label>
                            E-mail
                            <input name = 'email' required class="form-control" id = 'name' type = 'email' placeholder = 'E-mail' />
                            <small id="emailHelp" class="form-text text-muted">Tu email nunca se compartirá con nadie</small>
                        <label>
                    </div>
                    <div class="form-group row">
                        <label>
                            Contraseña
                            <input required name = 'password' class="form-control " id = 'password' type = 'password' placeholder = 'Contraseña' />
                        <label>
                    </div>
                    <button class="btn btn-primary" id = 'logIn' type = 'submit'>Iniciar sesión</button>
                </form>
                <div id = 'resultado'></div>
            </div>
        </div>
        <!--<script src="../js/check_login.js"></script>-->
    </body>
</html>