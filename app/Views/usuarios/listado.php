<?= $header; ?>
    <a href="crear" class="btn btn-success">Crear Usuario</a>
    <br/><br/>

        <table class="table table-light">
            <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Email</th>
                    <th>Tlf.</th>
                    <th>País</th>
                    <th>Campaign</th>
                    <th>Source</th>
                    <th>Medium</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($usuarios as $usuario){ ?>
                    <tr>
                        <td><?= $usuario['id'] ?></td>
                        <td><?= $usuario['nombre'] ?></td>
                        <td><?= $usuario['email'] ?></td>
                        <td><?= $usuario['telefono'] ?></td>
                        <td><?= $usuario['pais'] ?></td>
                        <td><?= $usuario['campaign'] ?></td>
                        <td><?= $usuario['source'] ?></td>
                        <td><?= $usuario['medium'] ?></td>
                        <td>
                            <a href = <?= 'editar/' . $usuario['id']; ?> class="btn btn-info" type="button">Editar</a>
                            <a  href = <?= 'borrar/' . $usuario['id']; ?> class="btn btn-danger" type="button">Borrar</a>
                        </td>
                    </tr>
                <?php }?>
            </tbody>
        </table>
<?= $footer ?>

    