<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

</head>
<body>
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Editar Usuario <?= $usuario['id']; ?></h5>
            <p class="card-text">
                <form method="post" action="../actualizar" enctype="multipart/form-data">
                    <input value = <?= $usuario['id'] ?>  type="hidden" name="id">

                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input value = "<?= $usuario['nombre'] ?>" id="nombre" class="form-control" type="text" name="nombre">
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input value = "<?= $usuario['email'] ?>" id="email" class="form-control" type="mail" name="email">
                    </div>

                    <div class="form-group">
                        <label for="email">Teléfono</label>
                        <input value = "<?= $usuario['telefono'] ?>" id="telefono" class="form-control" type="tel" name="telefono">  
                    </div>

                    <div class="form-group">
                        <label for="pais">País</label>
                        <input value = "<?= $usuario['pais'] ?>" id="pais" class="form-control" type="text" name="pais">  
                    </div>

                    <div class="form-group">
                        <label for="campaign">Campaign</label>
                        <input  value = "<?= $usuario['campaign'] ?>" id="campaign" class="form-control" type="text" name="campaign">  
                    </div>

                    <div class="form-group">
                        <label for="medium">Medium</label>
                        <select class="form-select" name="medium">
                            <?php switch($usuario['medium']){
                                case 'articulo': ?>
                                    <option value="articulo" selected>Artículo</option>
                                    <option value="newsletter" >Newsletter</option>
                                    <option value="banner">Banner</option>
                                    <?php break;
                                
                                case 'newsletter': ?>
                                    <option value="articulo" >Artículo</option>
                                    <option value="newsletter" selected >Newsletter</option>
                                    <option value="banner">Banner</option>
                                    <?php break;

                                case 'banner': ?>
                                    <option value="articulo" >Artículo</option>
                                    <option value="newsletter" >Newsletter</option>
                                    <option value="banner" selected>Banner</option>
                                    <?php break;?>

                            <?php }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="source">Source</label>
                        <select class="form-select" multiple name="source[]">
                    <?php 
                    /*$options = ['facebook', 'twitter', 'web', 'mailing'];
                    $sources = $usuario['source'];
                    $source = explode(', ', $sources);
                    
                    foreach($source as $item){

                    }*/
                         
                    ?>
                   
                            <option value="Facebook">Facebook</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Web">Web</option>
                            <option value="Mailing">Mailing</option>
                        </select>
                    </div>

                    <button class="btn btn-success" type="submit">Editar</button>
                </form>
            </p>
        </div>
    </div>
    <a href="../listado">Volver</a>

</body>
</html>