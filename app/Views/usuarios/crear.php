<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <?php if(session('mensaje')){ ?>
        <div class="alert alert-danger" role="alert">
            <?= session('mensaje'); ?>
        </div>
    <?php } ?>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Crear Usuario</h5>
            <p class="card-text">
                <form method="post" action="guardar" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input value = "<?= old('nombre'); ?>" id="nombre" class="form-control" type="text" name="nombre">
                    </div>

                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input value = "<?= old('email'); ?>" id="email" class="form-control" type="mail" name="email">
                    </div>

                    <div class="form-group">
                        <label for="telefono">Teléfono</label>
                        <input maxlength="9" value = "<?= old('telefono'); ?>" id="telefono" class="form-control" type="tel" name="telefono">  
                    </div>

                    <div class="form-group">
                        <label for="pais">País</label>
                        <input value = "<?= old('pais'); ?>" id="pais" class="form-control" type="text" name="pais">  
                    </div>

                    <div class="form-group">
                        <label for="campaign">Campaign</label>
                        <input value = "<?= old('pais'); ?>" id="campaign" class="form-control" type="text" name="campaign">  
                    </div>

                    <div class="form-group">
                        <label for="source">Source</label>
                        <select class="form-select" multiple name="source[]">
                        <option value="" selected>--- Selecciona una o varias opciones ---</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Twitter">Twitter</option>
                            <option value="Web">Web</option>
                            <option value="Mailing">Mailing</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="medium">Medium</label>
                        <select class="form-select" name="medium">
                            <option value="" selected>--- Selecciona una opción ---</option>
                            <option value="articulo">Artículo</option>
                            <option value="newsletter">Newsletter</option>
                            <option value="banner">Banner</option>
                        </select>
                    </div>
                    <br>
                    <button class="btn btn-success" type="submit">Guardar</button>
                </form>
            </p>
        </div>
    </div>

    
</body>
</html>