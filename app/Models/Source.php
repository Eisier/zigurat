<?php 
namespace App\Models;

use CodeIgniter\Model;

class Source extends Model{
    protected $table      = 'source';
    // Uncomment below if you want add primary key
    /*protected $primaryKey = 'id';
    protected $allowedFields = ['nombre', 'email', 'telefono', 'pais', 'source', 'medium', 'campaign'];*/
}